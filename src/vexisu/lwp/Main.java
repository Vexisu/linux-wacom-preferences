package vexisu.lwp;

import vexisu.lwp.devices.Device;
import vexisu.lwp.devices.DevicesManager;
import vexisu.lwp.gui.Window;

import javax.swing.*;
import java.io.BufferedReader;

/**
 * Created by vexisu on 17.03.17.
 */
public class Main
{

    public static void main(String[] args) throws Exception
    {
        init();
        SwingUtilities.invokeLater(() -> new Window("Linux Wacom Properties"));
    }

    private static void init() throws Exception
    {
        Terminal term = new Terminal();
        term.execute("xsetwacom", "--list", "devices");
        BufferedReader output1 = term.getOutput();
        String line;

        while ((line = output1.readLine()) != null)
        {
            String[] args = line.split("\t");
            int id = Integer.parseInt(args[1].replace("id: ", ""));
            String type = args[2].replace("type: ", "");
            Device device = new Device(args[0], id, type);
            DevicesManager.registerDevice(device);
        }

        for (Device device : DevicesManager.getDevices())
        {
            term.execute("xsetwacom", "--get", "" + device.getId(), "Area");
            BufferedReader output = term.getOutput();
            while ((line = output.readLine()) != null)
            {
                String area[] = line.split(" ");
                device.setArea(Integer.parseInt(area[0]), Integer.parseInt(area[1]), Integer.parseInt(area[2]), Integer.parseInt(area[3]));
            }
        }
        for (Device device : DevicesManager.getDevices())
        {
            term.execute("xsetwacom", "--set", "" + device.getId(), "ResetArea");
            Thread.sleep(10);
            term.execute("xsetwacom", "--get", "" + device.getId(), "Area");
            BufferedReader output = term.getOutput();
            while ((line = output.readLine()) != null)
            {
                String area[] = line.split(" ");
                device.setMaxArea(Integer.parseInt(area[0]), Integer.parseInt(area[1]), Integer.parseInt(area[2]), Integer.parseInt(area[3]));
            }
            term.execute("xsetwacom", "--set", "" + device.getId(), "Area", "" + device.getArea()[0], "" + device.getArea()[1], "" + device.getArea()[2], "" + device.getArea()[3]);
        }
    }
}
