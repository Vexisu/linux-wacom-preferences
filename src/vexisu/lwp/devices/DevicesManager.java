package vexisu.lwp.devices;

import java.util.ArrayList;

public class DevicesManager
{

    private static ArrayList<Device> devices = new ArrayList();

    public static ArrayList<Device> getDevices()
    {
        return devices;
    }

    public static void registerDevice(Device device)
    {
        devices.add(device);
        System.out.println("Registered: " + device.getName() + "(" + device.getId() + ") [" + device.getType() + "]");
    }

    public static Device getDeviceByID(int id)
    {
        for (Device device : devices)
        {
            if (device.getId() == id)
            {
                return device;
            }
        }
        return null;
    }

}
