package vexisu.lwp.devices;

public class Device
{

    private String name;
    private int id;
    private String type;
    private int[] area;
    private int[] maxarea;

    public Device(String name, int id, String type)
    {
        this.name = name;
        this.id = id;
        this.type = type;
    }

    public String getName()
    {
        return name;
    }

    public int getId()
    {
        return id;
    }

    public String getType()
    {
        return type;
    }

    public void setArea(int x, int y, int areax, int areay)
    {
        this.area = new int[]{x, y, areax, areay};
    }

    public int[] getArea()
    {
        return area;
    }

    public void setMaxArea(int x, int y, int areax, int areay)
    {
        this.maxarea = new int[]{x, y, areax, areay};
    }

    public int[] getMaxArea()
    {
        return maxarea;
    }

}
