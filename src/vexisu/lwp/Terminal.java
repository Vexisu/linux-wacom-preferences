package vexisu.lwp;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

/**
 * Created by vexisu on 17.03.17.
 */
public class Terminal
{
	
	private ProcessBuilder builder;
	private Process        process;
	
	public void execute(String... args) throws IOException
	{
		this.builder = new ProcessBuilder(args);
		this.builder.redirectErrorStream(true);
		this.process = this.builder.start();
	}
	
	public BufferedReader getOutput()
	{
		InputStream is = this.process.getInputStream();
		return new BufferedReader(new InputStreamReader(is));
	}
}
