package vexisu.lwp.gui;

import vexisu.lwp.devices.Device;

import javax.swing.event.MouseInputAdapter;
import java.awt.*;
import java.awt.event.MouseEvent;

public class Area extends Component
{
	
	int width;
	int height;
	private Device device;
	private int[]  grip;
	private int pressed = 0;
	
	public Area(Device device)
	{
		super();
		
		this.device = device;
		this.addMouseListener(new MouseInputAdapter()
		{
			@Override
			public void mousePressed(MouseEvent mouseEvent)
			{
				super.mousePressed(mouseEvent);
				System.out.println("! " + mouseEvent.getX() + " ! " + (Area.this.grip[0] - 5 + Area.this.grip[2]));
				
				if ((mouseEvent.getX() > ((Area.this.grip[0] - 5) + Area.this.grip[2])) &&
					(mouseEvent.getX() < (Area.this.grip[0] + 5 + Area.this.grip[2])) &&
					(mouseEvent.getY() > (Area.this.grip[1] - 5)) && (mouseEvent.getY() < (Area.this.grip[1] + 5)))
				{
					Area.this.pressed = 1;
				} else
				{
					Area.this.pressed = 2;
				}
				
				Thread thread = new Thread(() -> {
					while (Area.this.pressed == 1)
					{
						try
						{
							Thread.sleep(20);
						} catch (InterruptedException e)
						{
							e.printStackTrace();
						}
						int x = (device.getMaxArea()[2] * ((((int) MouseInfo.getPointerInfo().getLocation().getX() -
															 mouseEvent.getXOnScreen()) + mouseEvent.getX()) -
														   Area.this.grip[2])) / Area.this.width;
						int y = (device.getMaxArea()[3] * (((int) MouseInfo.getPointerInfo().getLocation().getY() -
															mouseEvent.getYOnScreen()) + mouseEvent.getY())) /
								Area.this.height;
						if ((device.getArea()[0] >= 0) && (x <= device.getMaxArea()[2]) && (device.getArea()[1] >= 0) &&
							(y <= device.getMaxArea()[3]) && (x > device.getArea()[0]) && (y > device.getArea()[1]))
						{
							device.setArea(device.getArea()[0], device.getArea()[1], x, y);
						}
						Area.this.repaint();
					}
					if (Area.this.pressed == 2)
					{
						final int area[] = device.getArea();
						int x = (device.getMaxArea()[2] * ((((int) MouseInfo.getPointerInfo().getLocation().getX() -
															 mouseEvent.getXOnScreen()) + mouseEvent.getX()) -
														   Area.this.grip[2])) / Area.this.width;
						int y = (device.getMaxArea()[3] * (((int) MouseInfo.getPointerInfo().getLocation().getY() -
															mouseEvent.getYOnScreen()) + mouseEvent.getY())) /
								Area.this.height;
						while (Area.this.pressed == 2)
						{
							try
							{
								Thread.sleep(20);
							} catch (InterruptedException e)
							{
								e.printStackTrace();
							}
							
							int x2 = (device.getMaxArea()[2] *
									  ((((int) MouseInfo.getPointerInfo().getLocation().getX() -
										 mouseEvent.getXOnScreen()) + mouseEvent.getX()) - Area.this.grip[2])) /
									 Area.this.width;
							int y2 = (device.getMaxArea()[3] *
									  (((int) MouseInfo.getPointerInfo().getLocation().getY() -
										mouseEvent.getYOnScreen()) + mouseEvent.getY())) / Area.this.height;
							int[] area2 = new int[] {(area[0] + x2) - x, (area[1] + y2) - y, (area[2] + x2) - x,
													 (area[3] + y2) - y};
							boolean inbound = true;
							if ((area2[0] < 0) || (area2[1] < 0))
							{
								inbound = false;
							}
							if (area2[2] > device.getMaxArea()[2])
							{
								inbound = false;
							}
							if (area2[3] > device.getMaxArea()[3])
							{
								inbound = false;
							}
							if (inbound)
							{
								device.setArea(area2[0], area2[1], area2[2], area2[3]);
							}
							Area.this.repaint();
						}
					}
				});
				thread.start();
			}
			
			@Override
			public void mouseReleased(MouseEvent mouseEvent)
			{
				super.mouseReleased(mouseEvent);
				Area.this.pressed = 0;
			}
		});
	}
	
	
	@Override
	public void paint(Graphics g)
	{
		super.paint(g);
		this.width = ((this.device.getMaxArea()[2] * 275) / this.device.getMaxArea()[3]);
		this.height = 275;
		int xoffset = this.getWidth() / 2 - this.width / 2;
		
		int areax = 0;
		if (this.device.getArea()[0] != 0)
		{
			
			areax = ((this.device.getArea()[0] * 275) / this.device.getMaxArea()[3]);
		}
		
		int areay = 0;
		if (this.device.getArea()[1] != 0)
		{
			areay = (this.device.getArea()[1] * this.width) / this.device.getMaxArea()[2];
		}
		
		
		int areax2 = ((this.device.getArea()[2] * 275) / this.device.getMaxArea()[3]);
		int areay2 = ((this.device.getArea()[3] * this.width) / this.device.getMaxArea()[2]);
		
		grip = new int[] {areax2, areay2, xoffset};
		
		g.setColor(new Color(255, 255, 255));
		g.fillRect(xoffset, 0, this.width, this.height);
		g.setColor(new Color(100, 100, 100));
		g.drawRect(xoffset, 0, this.width - 1, this.height - 1);
		g.setColor(new Color(0, 150, 255));
		g.drawRect(xoffset + areax, areay, areax2 - areax - 1, areay2 - areay);
		g.fillRect((xoffset + this.grip[0]) - 5, this.grip[1] - 5, 10, 10);
	}
	
	
}
