package vexisu.lwp.gui;

import vexisu.lwp.Terminal;
import vexisu.lwp.devices.Device;
import vexisu.lwp.devices.DevicesManager;

import javax.swing.*;
import javax.swing.filechooser.FileFilter;
import java.awt.*;
import java.io.*;
import java.util.Scanner;

/**
 * Created by vexisu on 17.03.17.
 */
public class Window extends JFrame
{
	
	private JPanel deviceslist;
	private JPanel areasettings;
	private JPanel options;
	
	private Area   area;
	private Device device;
	
	private JComboBox<String> devices;
	private JButton           proportions;
	private JButton           apply;
	private JButton           loadFromFile;
	private JButton           saveToFile;
	
	public Window(String name)
	{
		super(name);
		this.init();
		this.devicesList();
		this.areaSettings();
		this.options();
		this.setVisible(true);
	}
	
	private void init()
	{
		this.setSize(500, 480);
		this.setResizable(false);
		this.setLocationByPlatform(true);
		this.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
		this.setLayout(null);
	}
	
	private void devicesList()
	{
		this.deviceslist = new JPanel(new BorderLayout());
		this.devices = new JComboBox();
		for (Device device : DevicesManager.getDevices())
		{
			this.devices.addItem(device.getName() + "id: " + device.getId());
		}
		this.deviceslist.add(this.devices);
		this.deviceslist.setSize(500, 45);
		this.deviceslist.setLocation(0, 0);
		this.deviceslist.setBorder(BorderFactory.createTitledBorder("Device"));
		this.add(this.deviceslist);
		
		this.devices.addActionListener(actionEvent -> {
			String devicename = (String) this.devices.getSelectedItem();
			String[] args = devicename.split("id: ");
			System.out.println("Selected: " + args[1]);
			Window.this.areasettings.remove(this.area);
			Window.this.area = new Area(DevicesManager.getDeviceByID(Integer.parseInt(args[1])));
			Window.this.area.setLocation(5, 15);
			Window.this.area.setSize(490, 275);
			Window.this.areasettings.add(this.area);
			Window.this.areasettings.repaint();
			Window.this.device = DevicesManager.getDeviceByID(Integer.parseInt(args[1]));
		});
	}
	
	private void areaSettings()
	{
		this.areasettings = new JPanel(null);
		this.areasettings.setBorder(BorderFactory.createTitledBorder("Area"));
		this.areasettings.setSize(500, 295);
		this.areasettings.setLocation(0, 45);
		String devicename = (String) this.devices.getSelectedItem();
		String[] args = devicename.split("id: ");
		this.device = DevicesManager.getDeviceByID(Integer.parseInt(args[1]));
		this.area = new Area(this.device);
		this.area.setLocation(5, 15);
		this.area.setSize(490, 275);
		this.areasettings.add(this.area);
		this.add(this.areasettings);
	}
	
	private void options()
	{
		this.options = new JPanel(null);
		this.options.setBorder(BorderFactory.createTitledBorder("Options"));
		this.options.setSize(500, 100);
		this.options.setLocation(0, 340);
		
		this.proportions = new JButton("Set to correct proportions");
		this.proportions.setLocation(10, 20);
		this.proportions.setSize(480, 20);
		this.proportions.addActionListener(actionEvent -> {
			Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
			double width = screenSize.getWidth();
			double height = screenSize.getHeight();
			int ycp =
					((Window.this.device.getArea()[2] - Window.this.device.getArea()[0]) * (int) height) / (int) width;
			int xcp =
					((Window.this.device.getArea()[3] - Window.this.device.getArea()[1]) * (int) width) / (int) height;
			if ((Window.this.device.getArea()[2] - Window.this.device.getArea()[0]) > xcp)
			{
				Window.this.device.setArea(Window.this.device.getArea()[0], Window.this.device.getArea()[1],
										   (Window.this.device.getArea()[2] -
											(Window.this.device.getArea()[2] - Window.this.device.getArea()[0])) + xcp,
										   Window.this.device.getArea()[3]);
			} else
			{
				Window.this.device.setArea(Window.this.device.getArea()[0], Window.this.device.getArea()[1],
										   Window.this.device.getArea()[2], (Window.this.device.getArea()[3] -
																			 (Window.this.device.getArea()[3] -
																			  Window.this.device.getArea()[1])) + ycp);
			}
			this.area.repaint();
		});
		this.options.add(proportions);
		
		this.apply = new JButton("Apply");
		this.apply.setSize(180, 45);
		this.apply.setLocation(310, 45);
		this.apply.addActionListener(actionEvent -> {
			Terminal term = new Terminal();
			try
			{
				term.execute("xsetwacom", "--set", "" + Window.this.device.getId(), "Area",
							 "" + Window.this.device.getArea()[0], "" + Window.this.device.getArea()[1],
							 "" + Window.this.device.getArea()[2], "" + Window.this.device.getArea()[3]);
			} catch (IOException e)
			{
				e.printStackTrace();
			}
		});
		this.options.add(this.apply);
		
		FileFilter fileFilter = new FileFilter()
		{
			@Override
			public boolean accept(File file)
			{
				if (file.isDirectory())
				{
					return true;
				} else
				{
					if (file.getAbsolutePath().endsWith(".lwpconf"))
					{
						return true;
					}
				}
				return false;
			}
			
			@Override
			public String getDescription()
			{
				return "LWP config file";
			}
		};
		
		this.loadFromFile = new JButton("Load from file...");
		this.loadFromFile.setSize(295, 20);
		this.loadFromFile.setLocation(10, 45);
		
		this.loadFromFile.addActionListener(actionEvent -> {
			JFileChooser jFileChooser = new JFileChooser();
			jFileChooser.setFileSelectionMode(JFileChooser.FILES_ONLY);
			jFileChooser.setFileFilter(fileFilter);
			int returnValue = jFileChooser.showOpenDialog(Window.this);
			
			if (returnValue == JFileChooser.APPROVE_OPTION)
			{
				File selectedFile = jFileChooser.getSelectedFile();
				try
				{
					Scanner scanner = new Scanner(new FileInputStream(selectedFile));
					String[] settings = scanner.next().split(";");
					this.device.setArea(Integer.parseInt(settings[0]), Integer.parseInt(settings[1]),
										Integer.parseInt(settings[2]), Integer.parseInt(settings[3]));
					this.area.repaint();
				} catch (IOException e)
				{
					e.printStackTrace();
				}
			}
		});
		
		this.options.add(this.loadFromFile);
		
		this.saveToFile = new JButton("Save to file...");
		this.saveToFile.setSize(295, 20);
		this.saveToFile.setLocation(10, 70);
		
		this.saveToFile.addActionListener(actionEvent -> {
			JFileChooser jFileChooser = new JFileChooser();
			jFileChooser.setFileSelectionMode(JFileChooser.FILES_ONLY);
			
			jFileChooser.setFileFilter(fileFilter);
			jFileChooser.setSelectedFile(new File("config.lwpconf"));
			int returnValue = jFileChooser.showSaveDialog(Window.this);
			
			if (returnValue == JFileChooser.APPROVE_OPTION)
			{
				File selectedFile = jFileChooser.getSelectedFile();
				try
				{
					FileOutputStream fileOutputStream = new FileOutputStream(selectedFile);
					String settings = String.format("%d;%d;%d;%d", this.device.getArea()[0], this.device.getArea()[1],
													this.device.getArea()[2], this.device.getArea()[3]);
					fileOutputStream.write(settings.getBytes());
					fileOutputStream.close();
				} catch (IOException e)
				{
					e.printStackTrace();
				}
			}
		});
		
		this.options.add(this.saveToFile);
		
		this.add(this.options);
	}
	
}
